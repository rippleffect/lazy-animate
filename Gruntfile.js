module.exports = function (grunt) {

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        clean: {
            options: {
                force: true
            },
            files: [
                '<%= pkg.assetsPath %>',
                '<%= pkg.scriptPath %>'
            ]
        },

        bowercopy: {
            options: {
                srcPrefix: 'bower_components',
                clean: true
            },

            scripts: {
                options: {
                    destPrefix: '<%= pkg.scriptPath %>'
                },

                files: {
                    'jquery.min.js' : 'jquery/dist/jquery.min.js'
                }
            },

            css: {
                options: {
                    destPrefix: '<%= pkg.assetsPath %>'
                },

                files: {
                    '/vendor/animate.min.css' : 'animate.css/animate.min.css'
                }
            }
        },
    });


    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-bowercopy');
    grunt.registerTask('default', ['clean', 'bowercopy']);

};