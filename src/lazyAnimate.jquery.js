/**
 * lazyAnimation.js
 *
 * Dependencies:
 *   - jQuery 1.12.0+
 *   
 * @author John Hopley <jhopley@rippleffect.com>
 * @license GPL (http://www.gnu.org/licenses/gpl-3.0.html)
 * @version 1.0
 **/
;(function ($, window, undefined) {

	$.fn.lazyAnimate = function(options) {

        /**
         * @description Module default settings
         * @type {object}
         */
        var _settings = {
            watch              : 'load resize scroll',
            animationClass     : 'bounceIn',
            animationDuration  : '1s',
            animationEase      : 'ease',
            before             : null,
            after              : null,
            placeHolder        : null, // not done
            threshold          : 0,
            styles             : null, // before state
            visibleOnLoad      : true,
        }; 

        /**
         * @description constructor
         * @type {function}
         * @param $element {object}  
         * @return {function}          
         */
        function lazy($element) {
            this.$element = $element;
            return this.initialise();
        };
        
        /**
         * @description methods propotype
         * @type {object}
         */
        lazy.prototype = methods = {

            /**
             * @description jQuery window object 
             * @type {null|object}
             */
            $window: null,

            /**
             * @description Initilise on element
             * @type {function}
             * @param $element {object} 
             * @return {instance}
             */
            initialise: function() {

                if(!_settings.visibleOnLoad) {
                    this.$element.css({
                        opacity : 0
                    });
                }

                this.$window = (function(){
                    return $(window).trigger('lzy:start');
                })();

                this.registerEvents();

                return this.watch();
            },

            /**
             * @description Register events to instance & props
             * @type {function}
             * @return {void} 
             */
            registerEvents: function() {
                var $this = $(this);
                $this.on('lzy:before', _settings.before);
                $this.on('lzy:after', _settings.after);
                $this.on('lzy:start', this.initialise);
                this.$element.on('lzy:appear', this.appear);
            },

            /**
             * @decription Watches elememt on load, resize & scroll to check if is in the current view port
             * @type {function}
             * @return {void} 
             */
            watch: function() {
                var _instance = this;

                $(_instance).trigger('lzy:before');

                this.$window.on(_settings.watch, function(e){
                    if($.inViewPort(_instance.$element, _settings.threshold)) {
                        if(_instance.$element.is('img') && _instance.$element.attr('data-src') !== undefined) {
                            return _instance.defer();
                        }
                        return _instance.$element.trigger('lzy:appear');
                    }
                });
            }, 

            /**
             * @description swaps image sources, loads in the true data source then triggers the apear event.
             * @type {function}
             * @return {void} 
             */
            defer: function() {
                var _instance = this;
                var src = this.$element.attr('data-src');
                
                if(src) {
                    var $image = $('<img>', { src: src });

                    $image.on('load', function(){
                        _instance.$element.attr('src', src);
                        _instance.$element.trigger('lzy:appear');
                        $(_instance).trigger('lzy:after');
                    });
                }
            },

            /**
             * @description Applies class and animation attrs to element
             * @type {function}
             * @return {void} 
             */
            appear: function() {
                var $self = $(this);
                var animationClass = $self.attr('data-animation-class');
                var animationDuration = $self.attr('data-animation-duration');
                var animationEase = $self.attr('data-animation-ease');

                $self.removeAttr('style').css({
                    'animation-duration' : (animationDuration) ? animationDuration : _settings.animationDuration,
                    'animationEase': (animationEase) ? animationEase : _settings.animationEase,
                }).addClass((animationClass) ? animationClass : _settings.animationClass).trigger('lzy:after');

            },

        };

        if(options) {
            $.extend(_settings, options);
        }

		return this.each(function(e) {
            var $this = $(this);
            return new lazy($this);
		});

	};

} (jQuery, window, undefined));

/**
 * @description Checks if object is a function
 * @param object {object}  
 * @return {boolean}       
 */
$.isFunction = function(object) {
    return !!(object && object.apply && object.call);
};

/**
 * @description checks if element is the scope of the current viewport
 * @type {function}
 * @param element {object} element 
 * @return {boolean}         
 */
$.inViewPort = function(element, threshold) {
    var bounds = element[0].getBoundingClientRect();

    return Boolean(
        bounds.bottom > 0 &&
        bounds.right > 0 &&
        bounds.left < (window.innerWidth || $(window).width())-threshold &&
        bounds.top < (window.innerHeight || $(window).height())-threshold
    );
};

    